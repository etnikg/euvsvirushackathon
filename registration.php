<!DOCTYPE html>
<html lang="en">
<head>
<?php
    //session_start();
    include('index.php');
?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

</head>

<body>
<div style="margin:0 400px 0 400px;">
    <h2>Registration</h2>
    <form method="POST" action="validation.php">

        <div class="form-group">
            <label for="Name">Name:</label>
            <input style="font-size: 18px;" class="form-control" type="text"  id="name" placeholder="Enter name" name="name">
        </div>
        <div class="form-group ">
            <label for="LastName">Last name:</label>
            <input style="font-size: 18px;" class="form-control" type="text"  id="lastname" placeholder="Enter lastname" name="lastname">
        </div>
        <div class="form-group ">
            <label for="email">Email:</label>
            <input style="font-size: 18px;" class="form-control" type="email"  id="email" placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input style="font-size: 18px;" class="form-control" type="password"  id="pwd" placeholder="Enter password" name="password">
        </div>
        <div class="form-group">
            <button style="padding: 0 220px 0 220px;margin-top: 20px; font-size: 18px;" type="submit">Register</button>
        </div>
       
    </form>
</div>

</body>
</html>