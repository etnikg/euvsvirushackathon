<!DOCTYPE html>
<html lang="en">

<head>
<?php
    //session_start();
    include('index.php');
?>
</head>

<body>

<div class="top_widgets black">
    <section class="black">
        <div class="search-wrapper clearfix">
            <div class="zone clearfix">
            <div class="searchr block">
            <div id="default-search" >

            </div>
            <div id="advanced-search"  style="display: none;">

            <div class="row-fluid projects font_p" id="top_slide">
            <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner content">
            <div class="item active">
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj7">    
Purchase and localization of tools for distance education</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Ministry of Education</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj1.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj7"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="folder lorem lines">
                        <div id="project7" value="645" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 150px;">Purchase and localization of tools for distance education....</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project7">198</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>55</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>45</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj8">Advice to the Kosovo government regarding financing</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Kosovo government</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj2.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj8"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="folder lorem lines">
                        <div id="project8" value="132" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 120px;">Advice to the Kosovo government regarding financing and construction of healthcare facilities relating to the potential second wave of COVID-19 infections</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project8">900</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>253</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>20</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj9">Research into vaccinations against COVID-19</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Ministry of Health</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj3.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj9"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="folder lorem lines">
                        <div id="project9" value="765" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 150px;">Research into vaccinations against COVID-19</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project9">76</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>80</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>25</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj10">Advice to the Kosovo government regarding financing</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Kosovo government</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj4.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj10"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="folder lorem lines">
                        <div id="project10" value="125" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 120px;">Advice to the Kosovo government regarding financing and construction of healthcare facilities relating to the potential second wave of COVID-19 infections</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project10">900</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>253</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>20</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj11">Research into vaccinations against COVID-19</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Ministry of Health</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj5.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj11"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                   
                    <div class="folder lorem lines">
                        <div id="project11" value="876" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 150px;">Research into vaccinations against COVID-19</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project11">76</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>80</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>25</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
                <div class="span4 bordered">
                    <div class="folder"><h4 class="text-center title"><a href="projects.html#proj12">Purchase and localization of tools for distance education</a></h4></div>
                    <div class="proj_name">
                        <div class="clear"></div>
                        <span class="fleft author">Ministry of Education</span>
                        <table class="icons">
                            <tbody>
                            <tr>
                                <td class="icos_proj_sm bleft"><a class="group1" href="img/proj6.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                <td class="icos_proj_sm bleft"><a href="projects.html#proj12"><img src="img/icon_lock.png" alt=""></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="folder lorem lines">
                        <div id="project12" value="875" class="slider"></div>
                    </div>
                    <p class="descr" style="margin-bottom: 150px;">Purchase and localization of tools for distance education....</p>
                    <div class="folder price topbordered">
                        <div class="span4">
                            <strong class="green"><span>$</span><span class="project_value" href="#project12">200</span></strong>
                            <p>pledged</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>80</strong>
                            <p>backers</p>
                        </div>
                        <div class="span4 bleft">
                            <strong>25</strong>
                            <p>days to go</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
            </div>

            </div>


            </div>
            <div class="advanced-search-control">
                <a id="advanced-search-button" class="advanced-search-button fright adv_search latest expanded"></a>
            </div>
            </div>

            </div><!-- end of .zone-search -->
        </div><!-- end of .search-wrapper -->

        <div class="map-wrapper">

            <div id="map" class="map" style="position: relative; overflow: hidden; -webkit-transform: translateZ(0);height: 465px">
                <div id="home-page-featured">
                    <div id="Textus">
                    <div class="clear">
                    <p>WELCOME TO K-TEAM</p>
                    </div>
                    <div class="clear">
                    <p>Link sources of finance for COVID-19 </p>
                    </div>
                    </div>
                    <ul>
                        <li><a href="#"><img width="155" height="155" src="img/slide/01.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/02.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/03.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/04.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/05.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/06.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/07.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/08.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/09.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/10.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/11.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/12.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/13.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                    </ul>
                    <ul>
                        <li><a href="#"><img width="155" height="155" src="img/slide/14.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/15.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/16.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/17.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/18.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/19.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/20.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/21.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/22.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/23.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/01.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/02.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/03.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>

                    </ul>
                    <ul>
                        <li><a href="#"><img width="155" height="155" src="img/slide/16.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/07.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/08.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/10.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/11.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/02.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/03.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/13.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/14.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/15.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/17.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/18.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>
                        <li><a href="#"><img width="155" height="155" src="img/slide/19.jpg" class="attachment-post-thumbnail wp-post-image" alt=""></a></li>

                    </ul>
                    <!-- / container -->
                </div>
            </div>
        </div><!-- end of .map-wrapper -->
        <a name="id_about"></a>
    </section>
</div>
</div>

<div class="clear"></div>

<div class="content">
<h1 class="decoration text-center about"><span class="nobacgr">About</span></h1>
<div class="about_block">
    <p class="about_cont"><strong>Link sources of finance for COVID-19 related projects to those capable of designing and carrying out those projects</strong></p>
    <div class="row-fluid folders">
        <div class="span4">
            <div class="folder_bordered">
                <div class="folder"><a href="#"><img src="img/folder1.png" alt=""></a></div>
                <div class="folder"><h3 class="nomarg text-center">One :</h3></div>
                <div class="folder"><p class="green text-center"><strong>Call a project</strong></p></div>
            </div>
            <div class="folder"><img src="img/folder_n.png" alt=""></div>
        </div>
        <div class="span4">
            <div class="folder_bordered">
                <div class="folder"><a href="#"><img src="img/folder2.png" alt=""></a></div>
                <div class="folder"><h3 class="nomarg text-center">Two :</h3></div>
                <div class="folder"><p class="green text-center"><strong>Support a project</strong></p></div>
            </div>
            <div class="folder"><img src="img/folder_n.png" alt=""></div>
        </div>
        <div class="span4">
            <div class="folder_bordered">
                <div class="folder"><a href="#"><img src="img/folder3.png" alt=""></a></div>
                <div class="folder"><h3 class="nomarg text-center">Three :</h3></div> <a name="id3"></a>
                <div class="folder"><p class="green strong text-center"><strong>Fund a project</strong></p></div>

            </div>
            <div class="folder"><img src="img/folder_n.png" alt=""></div>
        </div>

    </div>
</div>

<h1 class="decoration text-center proj"><span class="nobacgr">Calls</span></h1>

<section class="section content" id="portfolio-list">

    <div class="wrapper row-fluid projects font_p" id="contentWrapper">
        <div class="zone-content clearfix">

            <div class="">

                <div class="portfolio-selection block">
                    <div class="decoration text-center" data-toggle="buttons-radio">
                        <div class="inline nobacgr">
                            <div class="text-center tags">
                                <ul id="portfolio-filter">
                                    <li><input class="btn" id="all" type="button" value="All" class="button-2-green current" title=""> </li>
                                    <li><input class="btn" id="creative" type="button" value="Education" class="button-2-green" title=""> </li>
                                    <li><input class="btn" id="developments" type="button" value="Architecture" class="button-2-green" title=""> </li>
                                    <li><input class="btn" id="fashion" type="button" value="Virology" class="button-2-green" title="">  </li>
                                    
                                    <li><input class="btn" id="other" type="button" value="Other" class="button-2-green" title="">   </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="portfolio-container">

                <div class="portfolio-listing creative development block" style="display: block;">
                    <div class="span4 bordered">
                        <div class="folder"><h4 class="text-center title"><a href="projects.html#proj1">Purchase and localization of tools for distance education</a></h4></div>
                        <div class="proj_name">
                            <div class="clear"></div>
                            <span class="fleft author"><h3>Ministry of Education</h3></span>
                            <table class="icons">
                                <tbody>
                                <tr>
                                    <td class="icos_proj_sm bleft"><a class="group1" href="img/proj1.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                    <td class="icos_proj_sm bleft"><a href="projects.html#proj1"><img src="img/icon_lock.png" alt=""></a></td>
                                </tr>
                                </tbody>
                             </table>
                            <div class="clear"></div>
                       </div>
                       
                        <div class="folder lorem lines">
                            <div id="project1" value="456" class="slider"></div>
                        </div>
                        <p class="descr" style="margin-bottom: 70px;">Purchase and localization of tools for distance education....</p>
                        <div class="folder price topbordered">
                            <div class="span4">
                                <strong class="green"><span>$</span><span class="project_value" href="#project1">198</span></strong>
                                <p>pledged</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>55</strong>
                                <p>backers</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>45</strong>
                                <p>days to go</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="portfolio-listing creative fashion identity other block" style="display: block;">
                    <div class="span4 bordered">
                        <div class="folder"><h4 class="text-center title"><a href="projects.html#proj2">Advice to the Kosovo government regarding financing</a></h4></div>
                        <div class="proj_name">
                            <div class="clear"></div>
                            <span class="fleft author"><h3>Kosovo government</h3></span>
                            <table class="icons">
                                <tbody>
                                <tr>
                                    <td class="icos_proj_sm bleft"><a class="group1" href="img/proj2.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                    <td class="icos_proj_sm bleft"><a href="projects.html#proj2"><img src="img/icon_lock.png" alt=""></a></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="folder lorem lines">
                            <div id="project2" value="325" class="slider"></div>
                        </div>
                        <p class="descr">Advice to the Kosovo government regarding financing and construction of healthcare facilities relating to the potential second wave of COVID-19 infections</p>
                        <div class="folder price topbordered">
                            <div class="span4">
                                <strong class="green"><span>$</span><span class="project_value" href="#project2">1000</span></strong>
                                <p>pledged</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>253</strong>
                                <p>backers</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>20</strong>
                                <p>days to go</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="portfolio-listing development other developments identity block" style="display: block;">
                    <div class="span4 bordered">
                        <div class="folder"><h4 class="text-center title"><a href="projects.html#proj3">Research into vaccinations against COVID-19</a></h4></div>
                        <div class="proj_name">
                            <div class="clear"></div>
                            <span class="fleft author"> <h3>Ministry of Health</h3></span>
                            <table class="icons">
                                <tbody>
                                <tr>
                                    <td class="icos_proj_sm bleft"><a class="group1" href="img/proj3.jpg" title=""><img src="img/icon_search.png" alt=""></a></td>
                                    <td class="icos_proj_sm bleft"><a href="projects.html#proj3"><img src="img/icon_lock.png" alt=""></a></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="folder lorem lines">
                            <div id="project3" value="555" class="slider"></div>
                        </div>
                        <p class="descr" style="margin-bottom: 70px;">Research into vaccinations against COVID-19 </p>
                        <div class="folder price topbordered">
                            <div class="span4">
                                <strong class="green"><span>$</span><span class="project_value" href="#project3">76</span></strong>
                                <p>pledged</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>80</strong>
                                <p>backers</p>
                            </div>
                            <div class="span4 bleft">
                                <strong>25</strong>
                                <p>days to go</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

           



        </div><!-- end of .zone-content -->
    </div><!-- end of .content-wrapper -->

</section>

<div class="btns">
<div class="btn-toolbar text-center">
    <div class="decoration text-center proj">
                <div class="btn-group nobacgr">
                    <button class="btn"><span class="gray">1</span></button>
                    <button class="btn mleft10">2</button>
                    <button class="btn mleft10">3</button>
                    <button class="btn mleft10">4</button>
                    <button class="btn mleft10">5</button>
                </div>
                <a name="id_reg" id="registr"></a>
    </div>

</div>

</div>
<h1 class="decoration text-center proj"><span class="nobacgr">Create Your Call</span></h1>
<div class="create_projects">
<div style="margin-left: 230px;" class="row-fluid">
    <div class="span3">
        <h4 class="text-center">Step 1: <span class="green">Registration</span></h4>
    </div>
    <div class="span3 bleft">
        <h4 class="text-center">Step 2: <span class="green">Call Summary</span></h4>
    </div>
    
</div>
<div style="margin-left: 230px;" class="row-fluid border_proj duration">
    <div class="span3 ">
        <div class="column_project">
            <h4 class="text-center">Personal Info:</h4>
            <form class="form-search" >
                <input type="text" class="inputp search-query span12" placeholder="Name">
                <input type="text" class="inputp search-query span12" placeholder="Surname">
                <input type="text" class="inputp search-query span12" placeholder="Email">
                <input type="text" class="inputp search-query span12" placeholder="Username">
                <input type="text" class="inputp search-query span12" placeholder="Password">
                <input type="text" class="inputp search-query span12" placeholder="Repeat Password">
                
                <input type="text" class="inputp search-query span12 face" style="padding-left: 30px;" placeholder="Account Name">
                <input type="text" class="inputp search-query span12 tweet" style="padding-left: 30px;" placeholder="Account Name">
            </form>
        </div>
    </div>
    <div class="span3">
        <div class="bleft column_project">
            <h4 class="text-center">Project Description:</h4>
            <form class="form-search">
                <input type="text" class="inputp search-query span12" placeholder="Project Name">
                <select class="">
                    <option>Category</option>
                    <option>Education</option>
                    <option>Architecture</option>
                    <option>Virology</option>
                    <option>Other</option>
                </select>
                <select class="">
                    <option>Location</option>
                    <option>Germany</option>
                    <option>Kosova</option>
                    <option>Italia</option>
                    <option>Albania</option>
                </select>
                
                <textarea id="redex2" class="inputp search-query span12" rows="7s" placeholder="Short description"></textarea>
                <input type="text" class="inputp search-query span12" placeholder="Payment Gateway">
            </form>
        </div>
    </div>
   

</div>


<div class="decoration text-center create_butt"><span class="nobacgr_whit"><button class="btn btn-large search-query" type="button"> <span class="butt_small"><strong>CREATE</strong></span></button></span><a name="id_cont"></a></div>

</div>


<h1 class="decoration text-center conts"><span class="nobacgr">Contact Us</span></h1>

<div class="contacts">
<div class="bborder">
    <p><strong>Send Us a <span class="green">Message</span></strong></p>
</div>
<form id="contact-us-form" class="row-fluid message" data-ajax="true">
    <div class="controls controls-row">
        <input class="inputp search-query span4" type="text" name="name" placeholder="Name*">
        <input class="inputp search-query span4" type="text" name="email" placeholder="Email*">
       
        <div class="clear"></div>
        <textarea id="redex" name="message" class="inputp search-query span12"  rows="5" placeholder=" Description"></textarea>
        <div class="clear"></div>
        <div class="tmargin20">
            <button type="button" class="btn" name="submit" value="submit" id="contact_submit"><strong>Send Message</strong></button>
        </div>
        <div id="error_placeholder" class="form-message text-green"></div>
    </div>
</form>
</div>

<div class="decoration text-center line_decor"></div>



</div>

<div id="footer">






<div class="footer_bot">
    <div class="footer_bott">
          <div class="fleft">© All Rights Reserved. Design by K-TEAM</div>
          <div class="fright">
            <ul id="social_b" class="socialbott inline">
                <li><a href="https://twitter.com/" target="_blank" original-title="twitter"><img src="img/social/icon_footer_face.png" alt=""></a></li>
                <li><a href="https://plus.google.com" target="_blank" original-title="googleplus"><img src="img/social/icon_footer_g.png" alt=""></a></li>
                <li><a href="https://www.facebook.com" title="facebook" target="_blank"><img src="img/social/icon_footer_tw.png" alt=""></a></li>
                <li><a href="http://www.linkedin.com/" target="_blank" original-title="linkedin"><img src="img/social/icon_footer_in.png" alt=""></a></li>
                <li><a href="http://www.linkedin.com/" target="_self" original-title="mail"><img src="img/social/icon_footer_p.png" alt=""></a></li>
                <li><a href="http://www.linkedin.com/" target="_self" original-title="mail"><img src="img/social/icon_footer_ball.png" alt=""></a></li>
            </ul>
          </div>
        <div class="clear"></div>
    </div>

    <script>
        /* <![CDATA[ */
        (function($) {
            $(function() {

                $('select').selectbox();

                $('#add').click(function(e) {
                    $(this).parents('div.section').append('<br /><br /><select><option>-- Выберите --</option><option>Пункт 1</option><option>Пункт 2</option><option>Пункт 3</option><option>Пункт 4</option><option>Пункт 5</option></select>');
                    $('select').selectbox();
                    e.preventDefault();
                })

                $('#add2').click(function(e) {
                    var options = '';
                    for (i = 1; i <= 5; i++) {
                        options += '<option>Option ' + i + '</option>';
                    }
                    $(this).parents('div.section').find('select').each(function() {
                        $(this).append(options);
                    })
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

                $('#che').click(function(e) {
                    $(this).parents('div.section').find('option:nth-child(5)').attr('selected', true);
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

            })
        })(jQuery)
        /* ]]> */

    </script>
    <script>
        /* <![CDATA[ */
        $(document).ready(function(){
            $("#toggl").click(function(){
                $("#panel").slideToggle("slow");
                $("#home-page-featured").slideToggle("slow");
                $(this).toggleClass('latest_top');
            });
        });
        /* ]]> */
        </script>
<script>
    /* <![CDATA[ */
    jQuery('#advanced-search').hide();
    jQuery('#advanced-search-button').click(function(event) {
        /* Preventing default link action */
        event.preventDefault();
        jQuery('#default-search').slideToggle('fast');
        jQuery('#advanced-search').slideToggle('fast');
        jQuery(this).toggleClass('expanded');
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function() {

        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $( "#autocomplete" ).autocomplete({
            source: availableTags
        });

        // Hover states on the static widgets
        $( "#dialog-link, #icons li" ).hover(
                function() {
                    $( this ).addClass( "ui-state-hover" );
                },
                function() {
                    $( this ).removeClass( "ui-state-hover" );
                }
        );
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    function scrollToAnchor(aid){
        var aTag = $("a[name='"+ aid +"']");
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }
    $("#link").click(function() {
        scrollToAnchor('id3');
    });
    $("#link_about").click(function() {
        scrollToAnchor('id_about');
    });
    $("#link_reg").click(function() {
        scrollToAnchor('id_reg');
    });
    $("#link_contact").click(function() {
        scrollToAnchor('id_cont');
    });
    $("#link_home").click(function() {
        scrollToAnchor('id_home');
    });
    $("#link_partners").click(function() {
        scrollToAnchor('id_partn');
    });
    $("#link_b").click(function() {
        scrollToAnchor('id3');
    });
    $("#link_about_b").click(function() {
        scrollToAnchor('id_about');
    });
    $("#link_reg_b").click(function() {
        scrollToAnchor('id_reg');
    });
    $("#link_contact_b").click(function() {
        scrollToAnchor('id_cont');
    });
    $("#link_partners_b").click(function() {
        scrollToAnchor('id_partn');
    });
    /* ]]> */


</script>
<script>
    /* <![CDATA[ */
    function hexFromRGB(r, g, b) {
        var hex = [
            r.toString( 16 ),
            g.toString( 16 ),
            b.toString( 16 )
        ];
        $.each( hex, function( nr, val ) {
            if ( val.length === 1 ) {
                hex[ nr ] = "0" + val;
            }
        });
        return hex.join( "" ).toUpperCase();
    }
    function refreshSwatch() {
        var red = $( "#project1" ).slider( "value" ),
                green = $( "#project2" ).slider( "value" ),
                blue = $( "#project3" ).slider( "value" ),
                blues = $( "#project4" ).slider( "value" ),
                hex = hexFromRGB( red, green, blue);
        $( "#swatch" ).css( "background-color", "#" + hex );

    }
    $(function() {
        $( "#project1, #project2, #project3, #project4, #project5, #project6, #project7, #project8, #project9, #project10, #project11, #project12" ).slider({
            orientation: "horizontal",
            range: "min",
            disabled: true,
            max: 100,
            slide: refreshSwatch,
            change: refreshSwatch
        });

        $.each($('.project_value'), function(k,v){
            var value = 100 * Number($(v).text())/Number($($(v).attr('href')).attr('value'));
            value = Math.floor(value);
            $($(v).attr('href')).slider("value", value);
            $($(v).attr('href')).find(".ui-slider-handle").text(value+'%');
        });

    });
    /* ]]> */

</script>

<script src="js/jquery.selectbox.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-uii.js"></script>
<script src="js/contact.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script src="js/jquery.placeholder.js"></script>
<script src="js/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/scripts.js"></script>

<!--[if IE 8]>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
<script>
    /* <![CDATA[ */
    $(document).ready(function(){
        var slider = $(".slider").slider({
            change: function () {
/*                var value = $(this).slider("option", "value");
                $(this).find(".ui-slider-handle").text(value+'%');*/
            },
            slide: function () {
/*                var value = $(this).slider("option", "value");
                $(this).find(".ui-slider-handle").text(value+"%");*/
            }
        });
/*        slider.each(function(index){
            var el = slider.get(index);
            var value = $(el).slider("value");
            $(el).slider("value", value);
        });*/
    });
    /* ]]> */
</script>
<script>
    /* <![CDATA[ */
    $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'nofollow',scalePhotos:'false'});

    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function() {
        $('input, textarea').placeholder();
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function(){
        // bind change event to select
        $('#dynamic_select').bind('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
    });
    /* ]]> */
</script>
</div>
</div>
</body>
</html>