# "K-Team" Project

## Project goal
Link sources of finance for COVID-19 related projects to those capable of designing and carrying out those projects

## Solution proposed
A software tool that enables sources of finance and others to enter information about COVID-19-related calls that they are sponsoring or of which they are aware into a searchable database which can then be accessed by entities and individuals that are interested in responding to such calls

## Solution's components
Front-end for information entry

Database permitting searching on a number of fields, e.g., sector (e.g., virology/architecture/education), countries eligible, deadline,  . .

Front-end for information seekers

Back end tying it all together

