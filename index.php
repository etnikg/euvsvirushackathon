
<!DOCTYPE html>
<html lang="en">
<?php
    session_start();
?>
<head>
    <meta charset="utf-8">
    <title>FUNDER - Bootstrap Crowdfunding Site</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700,200,300' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/resp.css" rel="stylesheet">
    <link href="css/colorbox.css" rel="stylesheet">

    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="img/touch-icon.png" />
    <link rel="image_src" href="img/touch-icon.png" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if IE 8]>
    <style type="text/css">
        input[type="checkbox"], input[type="radio"] {
            display: inline;
        }
        input[type="checkbox"] + label, input[type="radio"] + label{
            display: inline;
            background: none;
            margin-bottom: 15px;
        }
        .zone {
            background-color: #727272;
            filter: alpha(Opacity=70);
        }
        .black {
            background-color: #727272;
            filter: alpha(Opacity=70);
        }
    </style>
    <![endif]-->

    <script src="js/jquery.js"></script>
    <script>
        /* <![CDATA[ */
        $(window).scroll(function fade() {
            if ($(window).scrollTop() > 400) {
                $("#navtop").css("display", "block");
            }
            else {
                $("#navtop").css("display", "none");
            }
        });
        /* ]]> */

    </script>

</head>

<body>
<div id="header" class="header">
    <a name="id_home"></a>
        <div class="width titul">
            <div class="reg inline">
                <div class="bleft inline login"><a href="login.php" id="login-link" class="login-link">Login</a></div>
                <div class="bleft bright inline login"><a href="registration.php" id="reg-link" class="login-link">Register</a></div>
            </div>

            <div class="social inline fright">
                <ul id="social">
                <li class="bleft inline">
                    <ul style="margin-left: 10px">
    			    <li><a href="https://www.facebook.com" target="_blank"  original-title="twitter"><img src="img/social/icon_header_face.png" alt=""></a></li>
                        <li><a href="https://plus.google.com" target="_blank"  original-title="googleplus"><img src="img/social/icon_header_g.png" alt=""></a></li>
                        <li><a href="https://twitter.com/" title="facebook" target="_blank" ><img src="img/social/icon_header_tw.png" alt=""></a></li>
                        <li><a href="http://www.linkedin.com/" target="_blank" original-title="linkedin"><img src="img/social/icon_header_in.png" alt=""></a></li>
                     
                    </ul>
                </li>
                    <li class="bleft first socialz"><a class="active" href="#">EN</a></li>
                    <li class="bleft socialz"><a href="#">FR</a></li>
                    <li class="bleft socialz"><a href="#">IT</a></li>
                    <li class="bleft last socialz"><a href="#">DE</a></li>
                </ul>
            </div>
        </div>

        <div class="clear"></div>
        <div class="navigate">
            <div class="width">
                <div class="logo inline">
                    <h1><a  class="inline fleft" href="home.php">K-TEAM</a></h1>
                </div>

                <div class="navigation inline">
                    <ul class="nav inline menuleft">
                        <li class="active"><a class="bleft" href="index.html">HOME</a></li>
                        <li><a id="link_about" class="bleft" href="#">ABOUT</a></li>
                        <li><a id="link" class="bleft" href="#">CALLS</a></li>
                        <li><a id="link_reg" class="bleft" href="#">REGISTER</a></li>
                        <li><a id="link_contact" class="bleft" href="#">CONTACTS</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <div id="header_select">
                <select id="dynamic_select">
                    <option value="" selected>Select A Page</option>
                    <option value="index.html#id_about">About</option>
                    <option value="index.html#id3">Projects</option>
                    <option value="index.html#id_reg">Registration</option>
                    <option value="index.html#id_cont">Contacts</option>
                    <option value="index.html#id_partn">Partners</option>
                </select>
            </div>
        </div>


    </div>
<div id="navtop" class="width navigate" style="position: fixed;  display: none; top: 0px; left: 0px;right: 0px;z-index: 99999999999999999;">
    <div class="navigation">
        <ul class="nav inline menuleft">
            <li class="active"><a class="bleft" id="link_home" href="#">HOME</a></li>
            <li><a id="link_about_b" class="bleft" href="#">ABOUT</a></li>
            <li><a id="link_b" class="bleft" href="#">PROJECTS</a></li>
            <li><a id="link_reg_b" class="bleft" href="#">REGISTER</a></li>
            <li><a id="link_contact_b" class="bleft" href="#">CONTACTS</a></li>
            <li><a id="link_partners_b" class="bleft last" href="#">PARTNERS</a></li>
        </ul>
    </div>
</div>

<div class="contentt">
<div class="container-fluid line">

    <div class="slides">
            <div class="search_top">
                <div class="search" id="searchh">
                    <span class="white"><strong>Find calls:</strong></span>
                    <input id="autocomplete" title="" type="text" class="span7 search-query">
                    <input type="submit" class="submit" value="search" name="submit">
                </div>
            </div>
    </div>

    
    </div>
</div>

</body>
</html>

